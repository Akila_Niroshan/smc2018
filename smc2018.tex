\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{caption}
\usepackage{pdfpages}
\usepackage{booktabs}
\usepackage{hyperref}
\usepackage{tikz}
\usetikzlibrary{shapes}
\usepackage{verbatim}
\usepackage{pgfplots}
\usepackage{gensymb}
\pgfplotsset{width=8cm,height=3.5cm}
\usepackage{multirow}
\hypersetup{hidelinks=true}
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}

\newcommand\tab[1][0.4cm]{\hspace*{#1}}
\ifCLASSOPTIONcompsoc
\usepackage[caption=false,font=normalsize,labelfont=sf,textfont=sf]{subfig}
\else
\usepackage[caption=false,font=footnotesize]{subfig}
\fi
%\setlength{\belowcaptionskip}{-12pt}
\begin{document}

\title{Gait Analysis Using RGBD Sensors\\}

\author{\IEEEauthorblockN{First Last\IEEEauthorrefmark{1}, First Last\IEEEauthorrefmark{2}, First Last\IEEEauthorrefmark{3}, First Last\IEEEauthorrefmark{4},\\ First Last \IEEEauthorrefmark{5},  and First Last\IEEEauthorrefmark{6}}
\IEEEauthorblockA{Department of Electronic and Telecommunication Engineering, University of Moratuwa,\\Moratuwa, Sri Lanka}
\IEEEauthorblockA{Email: \IEEEauthorrefmark{1}name@somemail.com, \IEEEauthorrefmark{2}name@somemail.com, \IEEEauthorrefmark{3}name@somemail.com,\\\IEEEauthorrefmark{4}name@somemail.com,\IEEEauthorrefmark{5}name@somemail.com,\IEEEauthorrefmark{6}name@somemail.com}}

\maketitle
\thispagestyle{plain}
\pagestyle{plain}

\begin{abstract}
Human gait analysis, the study of human locomotion, is possible with low-cost RGBD sensors such as the Kinect sensor. However, due to the inherent depth sensing accuracy limitations of these sensors as the distance from the sensor increases, the distance range of gait analysis too becomes small an inefficient for clinical use. We present a system that uses two independent Kinects in a data fusion framework that increases the distance range of gait analysis from $2.5\:\mathrm{m}$ to $4\:\mathrm{m}$ with three gait cycles. Our gait parameters are reasonably accurate and comparable with existing systems with 4\% error in length measurements and $5 \degree$ error in flexion measurements.
\end{abstract}

\begin{IEEEkeywords}
Gait, Kinect, clinical, rehabilitation, diagnostic, vision based, gait parameters,
Kalman filter
\end{IEEEkeywords}

\section{Introduction}
Gait analysis is the study of human locomotion. The human walking pattern depends on the muscles, the joints and the nervous system \cite{m2}. Gait analysis is important in diagnosing patients, who are recovering from accidents, brain strokes, neurological diseases\cite{hausdorff2000dynamic}, musculoskeletal anomalies and psychiatric disorders\cite{hodgins2008importance}. Clinicians prescribe gait analysis test as a standard test to identify and monitor the progress of treatments for aforementioned disorders.

Gait analysing systems can be categorised as vision-based systems and non-vision-based systems. Vision-based systems are further categorised into marker-based systems and markerless systems. Marker-based systems \cite{a1} require wearing special cloths with markers, especially designed high performance hardware and software to operate. Due to these reasons marker-based systems are expensive. On the other hand, current implementations of markerless systems have a limited operating range which is not sufficient for gait analysis\cite{m8} \cite{fern2012biomechanical}. Even though a treadmill may be employed to overcome this limitation \cite{run3}\cite{pfister2014comparative}, forceful walking on a treadmill changes the natural walking pattern\cite{lee2008biomechanics}. Besides, patients who are very weak to walk on a treadmill (e.g., patients who walk on crutches) are not be able to use treadmill-based systems.

In this paper, as a solution for the aforementioned problems, we propose an extended-range real-time markerless three dimensional (3-D) gait analysis system using Microsoft Kinect V2 sensors. Extended-range is achieved using multiple Kinect sensors. Optimum placement of Kinect sensors (Fig. \ref{fig:Camera_setup}) and the use of an optimum fusion algorithm improve the accuracy of the system in comparison to similar implementations \cite{m8} while improving the operating range. Use of Kinect sensors has reduced the cost of hardware in comparison to marker based systems \cite{a1} while reducing the computational complexity.

\begin{figure}[htp]
	\centering
	\includegraphics[width=0.48\textwidth]{images/Camera_setup.pdf}
	\caption{Kinect sensor setup of our system}
	\label{fig:Camera_setup}
\end{figure} 

RGBD sensors, Kinects in particular, have been used for gait analysis, multiple Kinect sensor fusion for human skeleton tracking using Kalman filtering  by Sungphill \emph{et al.}\cite{m8} presents a weighted measurement fusion method that uses Kalman filter for multiple Kinect sensors. They uses five Kinect sensors placed in a semi-circular arch. This limits the system to a fixed operating range. Our solution is scalable for longer operating ranges depending on the requirement. We have achieved a similar level of performance accuracy with a lesser number of Kinect sensors. 

In section II we present Kinect camera calibration algorithm, real-time Kalman fusion algorithm and robust gait parameter calculation methodology. Empirical evaluation in the section III shows our results are robust and accurate when compared with the ground truth measurements and the readings obtained from a verified inertial measurement based gait analysis system\cite{a11}. Finally, we present conclusion in section IV.

\section{Proposed Multi-Kinect-Based Gait Analysis System}

\subsection{Overview}
\label{Overview}
In this section, we discuss the overall architecture of the proposed gait analysis system. Our system, shown in Fig. \ref{fig:Overview},  has two Kinect sensors. The  Kinect 1 is the master while the Kinect 2 is the slave. Each Kinect sensor is connected to a computer that has Microsoft SDK for Kinect to use as the interface. 

We have implemented the overall software system using five major modules. Sensor Calibration, the first block, addresses the coordinate system misalignment of the two Kinect sensors. Since the system has two Kinect sensors we use a data fusion module to combine the sensor outputs. The third is the gait parameter calculation engine. Fused data have $X$, $Y$ and $Z$ cartesian spatial coordinates of 25 human body points. Gait parameter calculation engine uses aforesaid fused data to calculate gait parameters. Last two modules are graphical user interface (GUI) and the database. The database is used to store the calculated gait parameters and patient’s history for future reference and disease diagnosis process. We discuss sensor calibration, data fusion, gait parameter calculation, GUI and database system in the following subsections.

\begin{figure}[htp]
	\centering
	\input{images/architecture.tex}
	\caption{Block diagram of the overall system}
	\label{fig:Overview}
\end{figure} 

\subsection{Sensor Calibration}
\label{Calib}
Each Kinect sensor works in separate 3-D coordinate systems. Hence, we have to use a calibration mechanism to calculate parameters to transform coordinates in different coordinate systems into a common 3D coordinate system. Our calibration module, based on Umeyamas algorithm [6] calculates the rotation matrix and the translation vector. In this process we use the master Kinect coordinate system as the common 3D coordinate system. This works in two steps as following. 

\begin {enumerate}
\item Detecting 3D points in a calibration object.
\item Calculating rotation matrix and translation vector.
\end{enumerate}

We use Umeyama's algorithm \cite{m13}, because it is more accurate, robust, and effective than the singular-value decomposition (SVD) based algorithm \cite{m11}.  Using this algorithm, it is possible to calculate rotation matrix $R$, translation vector $t$,  and the scaling factor $C$. These calculated parameters are used to transform 3-D coordinates of human body captured through one Kinect sensor to the other Kinect sensor's coordinate frame. After transformation, the two sets of coordinates are fused as explained in data fusion subsection. 

\subsection{Data Fusion}
\label{DataFusion}

In our system we use data fusion to the measurements where the vision of both the sensors overlap and also where the sensors operate solely. The Microsoft Kinect V2 body part recognition SDK \cite{m9} gives the position coordinates of 25 joints of the human body relative to its coordinate system in meters. After transforming these coordinates to the master Kinect coordinate system (Section \ref{Calib}) the measurements of each Kinect can be fused.


There is no control input for the tracked coordinate. Microsoft Kinect SDK detects 25 body-parts. The state of the $k^{\text{th}}$ body-part coordinate is ${\hat x}_k=\begin{bmatrix}x & \dot x & y & \dot y & z & \dot z\end{bmatrix}^T$. Therefore we have a $150\times1$ matrix. The matrix $\mathrm{A}$ is obtained by linear motion model equations, 
${\hat x}_k = {\hat x}_{k-1} + \dot {\hat x}_{k-1}\times dt$ and $\dot {\hat x}_k = \dot {\hat x}_{k-1}$, where $dt$ is the time between two frames ($\frac{1}{30}$ s). $P_k$ are the error covariance matrices and $Q$ is the process noise covariance matrix. The measurement noise covariance matrix $R$ and the observation matrix $H$ of size ($75\times150$) are involved in the correction stage. These depends on the distance from the sensor and hence should be altered \cite{m19}. $R$ is with respect to each Kinect. To convert into the coordinate system of master Kinect, $R$ of the slave Kinect is rotated using $R_{rot} \times R \times R_{rot}^T$ where, $R_{rot}$ is the rotation matrix of the slave Kinect w.r.t. the coordinate system of the master Kinect.

Some coordinates may be occluded by the body or an object, hence it would be indicated as low reliable coordinates by the Kinect SDK. The SDK gives value 2 for full accuracy, value 1 for 50\% accuracy and value 0 for zero accuracy. In our implementation, we have adjusted the $H$ matrix only to get the fully accurate coordinates in the measurement update stage. When there is no reliable measurement at a given time, Kalman prediction is given as the output for that coordinate. When only one Kinect has reliable data, we take only the reliable data of Kinects for fusion.

\subsection{Gait Parameter Calculation}

Gait parameters are mainly categorized under angels and distances. Stride length, step length, cadence and spine to knee distance are the distances, and knee-hip-elbow flexion/extension-lateral and anterior pelvic-tilt are angels.

According to the camera setup the subject walks towards the Kinects along the $z$-axis (Fig. \ref{fig:Stride-Step}). Stride length and the step length are calculate by identifying the resting position of both the legs along the $z$-axis, corresponding to plateaus of fig \ref{fig:Stride-Step}. 1-D derivative of the $z$ coordinates will extract the plateau points. Distance between two consecutive plateaus of the same leg produces the stride length. Distance between two consecutive plateaus for left leg and right produces the step length.

\begin{figure}[htp]
	\centering
	\includegraphics[width=0.5\textwidth]{images/Stride-Step.pdf}
	\caption{Stride length, step length}
	\label{fig:Stride-Step}
\end{figure} 

Cadence is the number of steps per minute. Cadence is calculated after obtaining the number of steps over the time difference. Knee flexion/extension is the movement around knee joint. This is represented by the angel ${\theta}_{kf}$ (Fig \ref{fig:kneeHip-vector}) \cite{m2, m4} and calculated using Eq. \ref{knee_flex}.

\begin{figure}[htp]
	\centering
	\includegraphics[height=4.5cm]{images/kneeHip-vector}
	\caption{Hip-knee ($\protect\overrightarrow{HK}$) vector, Knee-ankle ($\protect\overrightarrow{KA}$) vector and knee flexion/extension ($\protect{\theta}_{kf}$).}
	\label{fig:kneeHip-vector}
\end{figure} 

\begin{equation} 
\label{knee_flex}
{\theta}_{kf} = \arccos \left \{ \frac{\overrightarrow{HK}\cdot\overrightarrow{KA}}{\left \| \overrightarrow{HK} \right \|\left \| \overrightarrow{KA} \right \|} \right \}.
\end{equation}

Hip flexion/extension is represent by the angle between a leg and the vertical coronal plane of the body. Hip flexion/extension is defined with respect to both the legs. It is calculated by getting the angle between vertical vector downwards ($\hat{v}$)  through the coronal plane and the vector representing each leg. With respect to the vector used, left and right hip flexion $\theta_{hf}$, lateral pelvic tilt $\theta_{lat}$ and anterior pelvic tilt  $\theta_{ant}$ calculates as Eq. \ref{hip_flex}, Eq. \ref{lateral_tilt} and  Eq. \ref{anterior_tilt}, where $B$ is the co-ordinate of the spine base and $M$ is the co-ordinate of the spine mid.

\begin{equation} 
\label{hip_flex}
\theta_{hf} = \arccos \left \{ \frac{\overrightarrow{KH}\cdot\hat{v}}{\left \| \overrightarrow{KH} \right \|\left \| \hat{v} \right \|} \right \}
\end{equation}

\begin{equation} 
\label{lateral_tilt}
\theta_{lat}=90^\circ-\arccos \left \{ \frac{\overrightarrow{BH}\cdot \overrightarrow{BM}}{\left \| \overrightarrow{BH} \right \|\left \| \overrightarrow{BM} \right \|} \right \}
\end{equation}

\begin{equation} 
\label{anterior_tilt}
\theta_{ant} = \arccos \left \{ \frac{\overrightarrow{BM}\cdot\hat{u}}{\left \| \overrightarrow{BM} \right \|\left \| \hat{u} \right \|} \right \}
\end{equation}

Due to injuries or diseases patients tend to show waddling gait (legs move in a circular path when walking), circumduction (conical movement of limb). Observation of spine mid to knee distance will indicates those abnormalities. The system can calculates the lateral distances between knees using the joint 3D coordinates on the frontal plane. The $x$, $y$, $z$ coordinates are passed to the gait parameter calculator with standard errors. Propagation of errors is calculated using standard propagation error equations \cite{m24}.

\section{Results and Verification}

In this section, we first describe the results pertaining to the individual blocks of our system: system calibration, data fusion, and gait parameter calculation. Then we present the results that verify the performance of the system along with comparisons.

\subsection{System Calibration}

We used 49 distinct corners of a vertically positioned $7\times7$ chess board to find the rotation $R$, translation $t$ and scaling factor $C$ using the Umeyama's algorithm. We computed the re-projection error for 24 points out of the 49 using the estimated $R$, $C$, and $t$. The root mean square (RMS) re-projection error was $10^{-6}\:\mathrm{m}$ and the maximum re-projection error was $5\times 10^{-3}\:\mathrm{m}$. Furthermore, rotation matrices were orthonormal and the scaling factor was 0.99 which should ideally be 1. RMS re-projection error for randomly selected samples of data turned out to be $10^{-2}\:\mathrm{m}$. This indicate that the calibration is reasonably accurate.

\subsection{Data Fusion}

Fig. \ref{fig:fus}  shows the gait data using only the individual Kinects and the fused gait data for two random coordinates. Spurious null values are due to frame losses. It is clear that the fused data follows very smoothly over the data from both the Kinects, though there are noise peaks in the data set. In addition, we can see that the calibration is successful, since data form both Kinects substantially overlap on each other after transformation.

\begin{figure}[htp]
	\centering
	\includegraphics[width=8.6cm]{images/resultFusion.pdf}
	\caption{Gait data of a random coordinates}
	\label{fig:fus}
\end{figure}

\subsection{Gait Parameter Calculation}

Table \ref{table:tab_one} shows the gait parameter output from our system and from clinical gait analysis \cite{m25} for an average data set. This comparison is to investigate whether our system gives meaningful gait parameters. We observe in the average data set that the stride length is almost as twice as the step length in the averaged system. In our system too those values have similar association.

\setlength{\arrayrulewidth}{1mm}
\setlength{\tabcolsep}{18pt}
\renewcommand{\arraystretch}{1.5} 

\begin{table}
	\centering
	\caption{Parameter comparison table}
	\renewcommand{\arraystretch}{1.0}
	\begin{tabular}{@{}lll@{}}
		\toprule
		Parameter & Our system & Averaged system \\
		\midrule
		Stride length & $1.1\:\mathrm{m}$ &$0.76\:\mathrm{m}-0.84\:\mathrm{m}$ \\
		Step length & $0.5\:\mathrm{m}$  & $0.41\:\mathrm{m}-0.45\:\mathrm{m}$ \\
		Cadence &$84.6\:\mathrm{steps/min}$ & $82-88\:\mathrm{steps/min}$ \\
		Speed    &$0.98\:\mathrm{m/s}$ & $0.54\:\mathrm{m/s}-0.6\:\mathrm{m/s}$ \\
		\bottomrule
	\end{tabular}
	
	\label{table:tab_one}
\end{table}

Fig. \ref{fig:knee_flex} shows the knee flexion graph obtain by our system (top plot) and the standard plot (bottom plot) \cite{m3}. It is possible to interpret our systems output by comparing with this reference qualitatively. Fig. \ref{fig:hip_flex} and \ref{fig:knee_spine_kinect} show the hip flexion and spine-mid to knee distance of a healthy person respectively along with the standard curves. The shape and periodicity of the hip flexion graphs (Fig. \ref{fig:hip_flex}) match with the standard curve. We do not report the lateral and anterior pelvic tilts to keep the presentation concise. 
Spine-mid to knee distance is a new parameter which is calculated by our system. A periodically changing pattern is the expectation from this graph and the validation can be directly done by measuring the distance manually.

\begin{figure}[htp]
	\centering
	\includegraphics[width=8cm]{images/kneeflex.pdf}
	\caption{Knee flexion-extension: our system vs. standard (source: wwrichard.net)}
	\label{fig:knee_flex}
\end{figure} 

\begin{figure}[htp]
	\centering
	\includegraphics[width=8cm]{images/hipflexion.pdf}
	\caption{Hip flexion-extension: our system vs. standard (source: musculoskeletalkey.com)}
	\label{fig:hip_flex}
\end{figure} 

\begin{figure}[htp]
	\centering
	\includegraphics[width=8cm]{images/spine_mid_to_knee.pdf}
	\caption{Spine mid to knee distance (our system)}
	\label{fig:knee_spine_kinect}
\end{figure} 

\begin{figure*}[htp]
	\centering
	\includegraphics[width=18.2cm]{images/LRH.pdf}
	\caption{(a) Knee flex/ext, (b) Hip flex/ext, (c) Elbow flex/ext, (d) Lateral pelvic tilt, (e) Anterior pelvic tilt, (f) Knee to spine mid (for patient data)}
	\label{fig:LRH_knee}
\end{figure*} 

Fig. \ref{fig:LRH_knee} is a set of gait parameter graphs of a patient suffering from Cerebral Palsy having a waddling, circumduction and shuffling in the walking. Medical doctors and physiotherapists indicated that these graphs qualitatively match with the patients walking pattern.

\subsection{Verification Experiments}
\subsubsection{Depth measurement of static objects}

Initial experiment was to check the depth measurement accuracy of the Kinect against the ground truth. We kept static objects infront of the Kinect sensor and measured the depth manually. Then using the \textit{coordinate mapper} functions of Kinect SDK, we obtain the depth measurements for interested points from the Kinect sensor. We took a sample of 504 data points over a range of $280\:\mathrm{cm}$.  Depth error of the Kinect increases with the increase of distance from the Kinect and the overall average depth error was be $4\:\mathrm{mm}$.

\subsubsection{Human body part length and human body joint angle measurements of a stationary person}

This experiment was conducted taking Kinect measurements while a person was standing motionless in front of the Kinect. The body part length detection accuracy was found by comparing the body part lengths obtained from the Kinect body part recognition, depth measurements obtained from the \textit{coordinate mapper} function and the actual physical length measurements between joints using a meter ruler. Distances were calculated for the upper arm, forearm, shin and the thigh. The body part angle detection error was found by comparing the angles between body parts calculated using Kinect body part co-ordinates and actual body angles measured using the Goniometer. The right-knee, left-knee and the elbow flexion-extension were calculated in degrees.

\begin{figure}[htp]
	\centering
	\input{images/bodyLength.tex}
	\caption{Average body part length detection error.}
	\label{fig:bodyLength}
\end{figure}

\begin{figure}[htp]
	\centering
	\input{images/jointAngle.tex}
	\caption{Maximum and Average human joint angle detection error}
	\label{fig:jointAngle}
\end{figure}
Measurements were made from $1\:\mathrm{m}$ to $3.2\:\mathrm{m}$ from the Kinect with $20\:\mathrm{ cm}$ step. The human body part length detection error of the Kinect system averages to $35\:\mathrm{mm}$ and averaged angle detection error was $4 \degree$ (Fig: \ref{fig:bodyLength}, \ref{fig:jointAngle}). The idea of data fusion is to increase the range from $2.5\:\mathrm{m}$ to $4.0\:\mathrm{m}$. Therefore there will be only a limited range of $1.5\:\mathrm{m}$ in which both the sensors gets data simultaneously. Fig. \ref{fig:fusion} shows that data fusion has increased the accuracy of the system, 5\% in body length detection and 7\% in joint angle detection.


\begin{figure}[htp]
	\centering
	\subfloat[Body part length detection average error]{
		\input{images/fusion_length.tex}
	}
	\\
	\subfloat[Joint angle detection average error]{
		\input{images/fusion_angle.tex}
	}
	\caption{Length and angle detection error before and after data fusion.}
	\label{fig:fusion}
\end{figure}

\subsubsection{Human body joint angle measurement of a moving character}

Dynamic joint angles were measured against the readings from Kairos Sensing \cite{a11}, which is a system built based on inertial measurement unit sensors. It has been verified against a robotic arm and has a maximum error of $3 \degree$. As shown in Fig. \ref{fig:Kairos}, the deviation of knee flexion and the hip flexion is $5 \degree$, when compared with the Kairos Sensing system.

\begin{figure}[htp]
	\centering
	\input{images/Kairos.tex}
	\caption{Average and maximum deviation of joint angles obtained by Kairos Sensing}
	\label{fig:Kairos}
\end{figure}

\begin{table}[htp]
	\centering
	\caption{Average difference of stride lengths}
	\renewcommand{\arraystretch}{1.0}
	\begin{tabular}{@{}p{4.0cm}c@{}}
		\toprule
		Parameter & Error \\
		\midrule
		Left stride & $5.0\:\mathrm{cm}$\\
		Right stride & $5.8\:\mathrm{cm}$  \\
		Step length & $4.8\:\mathrm{cm}$ \\
		\bottomrule
	\end{tabular}
	
	\label{fig:vid}
\end{table}

\subsubsection{Step and stride length detection of a moving character}
Step and stride length are the two main gait parameters to identify any abnormalities in the walking pattern. In the verification process, step and stride length were captured by a video footage and compared with the results obtained by our system. We found that the overall average error as $5\:\mathrm{cm}$. The smallest measurement of the used measuring tape is $1\:\mathrm{cm}$.



\subsection{Results comparison}
\begin{table}
	\centering
	\caption{Results comparison}
	\renewcommand{\arraystretch}{1.0}
	\label{tab:results-comparison}
	\begin{tabular}{@{}p{1.8cm}ccp{0.2cm}c@{}}
		\toprule
		& \multicolumn{2}{c}{Stride length/length error} & \multirow{2}{*}{FE} &  \\ 
		\cmidrule(lr){2-3}
		& ME        & EP        &  &  \\ 
		\midrule
		Multiple Kinect sensor fusion & $0.069\:\mathrm{m}$ & $6\%$ & $N/A$ & \\
		Biomechanical validation & $N/A$ & $N/A$ & $7^{\circ}$ &  \\ 
		2D markerless Gait analysis & $0.04\:\mathrm{m}$ & $3\%$ & $4.53^{\circ}$ & \\
		Our system & $0.05\:\mathrm{m}$ & $4\%$ & $5^{\circ}$ & \\
		\bottomrule
	\end{tabular}
\end{table}

In Table \ref{tab:results-comparison}, mean error (ME) in meters, error percentage (EP) and flexion error (FE) in degrees are compared. Moon \emph{et al.} \cite{m8} is the most closely related implementation to ours. Due to our optimal camera placement, we have achieved a similar, slightly superior performance with less number of cameras. Bio mechanical validation \cite{fern2012biomechanical} by Ferandez-Baena \emph {et al.} is a validation done for single Kinect system. Use of multiple Kinects and running the Kalman filter on the data have improved our accuracy over a single Kinect system. 2-D markerless gait analysis \cite{castelli20152d} by Castelli \emph {et al.} is a 2-D vision based system. Their error performance is more superior to our system. However, due to very high computational demand required for their point cloud projections their system is not real-time like our system.


\section{Conclusion}
As we have shown in our work, use of multiple Kinects and fusing the data using the Kalman filter has increased the operating range of $2.5\:\mathrm{ m}$ of a single Kinect to $4\:\mathrm{ m}$ while increasing the accuracy. This is achieved using only two Kinects. 
Average calibration ME is ${10}^{-6}\:\mathrm{ m}$ while the back projection error is ${5  \times {10}^{-3}}\:\mathrm{ m}$ which are well under the acceptable level of accuracy.
Verification for a static body reveals an error of $35\: \mathrm{  mm}$ in length measurements and $4 \degree$ in angle measurements. Results for a dynamic body in comparison with Kairos sensing gives a variation of $5 \degree$. The maximum reported propagation error of our system is $7 \degree$. These errors and the variations are within the acceptable level for gait analysis applications. 
Capture range of this system could be easily improved using a cascade of sensors.

\bibliography{references}
\bibliographystyle{ieeetran}

\end{document}
